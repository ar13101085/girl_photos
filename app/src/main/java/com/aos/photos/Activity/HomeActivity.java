package com.aos.photos.Activity;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.aos.photos.Adapter.CategorieAdapter;
import com.aos.photos.Helper.GridSpacingItemDecoration;
import com.aos.photos.Model.Category;
import com.aos.photos.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

/**
 * Created by Arif on 8/1/2017.
 */

public class HomeActivity extends AppCompatActivity {
    FirebaseStorage firebaseStorage=FirebaseStorage.getInstance();
    FirebaseDatabase firebaseDatabase=FirebaseDatabase.getInstance();
    DatabaseReference rootRefCategory;
    DatabaseReference rootRefPhoto;
    RecyclerView recycle_view;
    ArrayList<Category> categoryList=new ArrayList<Category>();
    CategorieAdapter categorieAdapter;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        System.out.println("Loading success...");
        setContentView(R.layout.recycle_view_category);
        recycle_view= (RecyclerView) findViewById(R.id.recycle_view);


        rootRefCategory=firebaseDatabase.getReference("category");
        rootRefPhoto=firebaseDatabase.getReference("photos");
        StorageReference root=firebaseStorage.getReference("img");
        categorieAdapter=new CategorieAdapter(categoryList,this);

        LinearLayoutManager layoutManager= new LinearLayoutManager(HomeActivity.this, LinearLayoutManager.VERTICAL, false);
        recycle_view.setLayoutManager(layoutManager);
        recycle_view.setAdapter(categorieAdapter);





        rootRefCategory.orderByKey().addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot s:dataSnapshot.getChildren()
                        ) {

                    Category category=s.getValue(Category.class);

                    categoryList.add(category);

                    System.out.println("Category found..." +category);
                }
                categorieAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
