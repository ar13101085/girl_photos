package com.aos.photos.Activity;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.menu.ActionMenuItemView;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.aos.photos.Model.Category;
import com.aos.photos.Model.Photos;
import com.aos.photos.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;

public class MainActivity extends AppCompatActivity {

    Button btn_add_photo;
    FirebaseStorage firebaseStorage=FirebaseStorage.getInstance();
    final StorageReference storageRef=firebaseStorage.getReference("img");
    FirebaseDatabase db=FirebaseDatabase.getInstance();
    DatabaseReference photoRef=db.getReference("photos");
    DatabaseReference categoryRef=db.getReference("category");
    ImageView imageView;
    Spinner spinner;
    File selectedFile;
    ArrayList<String> categoryAdapterData=new ArrayList<String>();
    ArrayAdapter<String> categoryAdapter;
    ImageView imageViewAddCategory;
    ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        categoryAdapter=new ArrayAdapter<String>(MainActivity.this,android.R.layout.simple_list_item_1,categoryAdapterData);
        btn_add_photo= (Button) findViewById(R.id.button_add);
        imageView= (ImageView) findViewById(R.id.imageView);
        imageViewAddCategory= (ImageView) findViewById(R.id.imageViewAddCategory);
        spinner= (Spinner) findViewById(R.id.spinner);
        progressBar= (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);
        spinner.setAdapter(categoryAdapter);

        EasyImage.configuration(this)
                .setImagesFolderName("AOS") //images folder name, default is "EasyImage"
                //.saveInAppExternalFilesDir() //if you want to use root internal memory for storying images
                .saveInRootPicturesDirectory(); //if you want to use internal memory for storying images - default





        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EasyImage.openGallery(MainActivity.this,0);

            }
        });

        btn_add_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                uploadPhoto();
            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},121);
        }


        categoryRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                System.out.println(dataSnapshot.toString());
                categoryAdapterData.clear();
                //GenericTypeIndicator<List<Category>> listCategory=new GenericTypeIndicator<List<Category>>(){};
                //List<Category> categoryList=new ArrayList<Category>();
                for (DataSnapshot s:dataSnapshot.getChildren()
                        ) {
                    //categoryList.add(s.getValue(Category.class));
                    Category category=s.getValue(Category.class);
                    categoryAdapterData.add(category.categoryName);
                }
                categoryAdapter.notifyDataSetChanged();
                //System.out.println("_______________________ "+categoryList.size());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        imageViewAddCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog=new Dialog(MainActivity.this);
                dialog.setContentView(R.layout.add_category);
                final EditText editTextName=dialog.findViewById(R.id.editTextName);

                Button buttonAddCategory=dialog.findViewById(R.id.buttonAddCategory);

                buttonAddCategory.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(editTextName.getText().toString().length()<4){
                            Toast.makeText(MainActivity.this, "Category Name size greater than three..", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        addToCategory(editTextName.getText().toString());
                        dialog.dismiss();
                    }
                });

                dialog.setCanceledOnTouchOutside(true);
                dialog.show();
            }
        });

        //addToCategory("Bangladeshi Girl");
    }

    public void addToCategory(String categoryName){
        Category category=new Category(categoryName);
        DatabaseReference keyRef=categoryRef.push();
        keyRef.setValue(category);
    }

    public void uploadPhoto(){
        if(selectedFile==null){
            Toast.makeText(this, "Please select a file for upload...", Toast.LENGTH_SHORT).show();
            return;
        }
        progressBar.setVisibility(View.VISIBLE);
        DatabaseReference keyRef=photoRef.child(spinner.getSelectedItem().toString()).push();

        Photos photos=new Photos(keyRef.getKey()+"."+FilenameUtils.getExtension(selectedFile.getName()),0,System.currentTimeMillis());

        keyRef.setValue(photos);

        InputStream stream = null;
        try {
            stream = new FileInputStream(selectedFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        StorageReference fileNameRef=storageRef.child(keyRef.getKey()+"."+FilenameUtils.getExtension(selectedFile.getName()));
        UploadTask uploadTask = fileNameRef.putStream(stream);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle unsuccessful uploads
                progressBar.setVisibility(View.GONE);
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                Uri downloadUrl = taskSnapshot.getDownloadUrl();
                System.out.println(downloadUrl.getPath());
                progressBar.setVisibility(View.GONE);
                Toast.makeText(MainActivity.this, "Successfully uploaded", Toast.LENGTH_SHORT).show();

            }

        }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                System.out.println("Upload is " + progress + "% done");
                progressBar.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {

            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                //Some error handling
            }

            @Override
            public void onImagePicked(File imageFile, EasyImage.ImageSource source, int type) {
                selectedFile=imageFile;
                Glide.with(MainActivity.this)
                        .load(imageFile)
                        .into(imageView);

            }
        });
    }
}
