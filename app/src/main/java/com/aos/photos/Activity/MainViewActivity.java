package com.aos.photos.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.aos.photos.Adapter.PhotoAdapter;
import com.aos.photos.Helper.GridSpacingItemDecoration;
import com.aos.photos.Model.Category;
import com.aos.photos.Model.Photos;
import com.aos.photos.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

public class MainViewActivity extends AppCompatActivity {
    FirebaseStorage firebaseStorage=FirebaseStorage.getInstance();
    FirebaseDatabase firebaseDatabase=FirebaseDatabase.getInstance();
    DatabaseReference rootRefCategory;
    DatabaseReference rootRefPhoto;
    RecyclerView recycle_view_photo;
    ArrayList<Photos> photoList=new ArrayList<Photos>();
    PhotoAdapter photoAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_view_layout);
        recycle_view_photo= (RecyclerView) findViewById(R.id.recycle_view_photo);


        rootRefCategory=firebaseDatabase.getReference("category");
        rootRefPhoto=firebaseDatabase.getReference("photos");
        StorageReference root=firebaseStorage.getReference("img");
        photoAdapter=new PhotoAdapter(photoList,root,MainViewActivity.this);

        //LinearLayoutManager horizontalLayoutManagaer= new LinearLayoutManager(MainViewActivity.this, LinearLayoutManager.VERTICAL, false);
        GridLayoutManager gridLayoutManager=new GridLayoutManager(this,3);
        recycle_view_photo.addItemDecoration(new GridSpacingItemDecoration(3,20,false));
        recycle_view_photo.setLayoutManager(gridLayoutManager);
        recycle_view_photo.setAdapter(photoAdapter);





        rootRefCategory.orderByKey().addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot s:dataSnapshot.getChildren()
                        ) {

                    Category category=s.getValue(Category.class);

                    rootRefPhoto.child(category.categoryName).limitToFirst(6).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            for (DataSnapshot photos:dataSnapshot.getChildren()
                                    ) {
                                Photos p=photos.getValue(Photos.class);
                                photoList.add(p);
                                System.out.println("value found..."+p.toString());
                            }
                            photoAdapter.notifyDataSetChanged();
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }
}
