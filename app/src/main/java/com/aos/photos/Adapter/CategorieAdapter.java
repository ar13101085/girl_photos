package com.aos.photos.Adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aos.photos.Fragment.CategoryListFragment;
import com.aos.photos.Helper.GridSpacingItemDecoration;
import com.aos.photos.Model.Category;
import com.aos.photos.Model.Photos;
import com.aos.photos.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

public class CategorieAdapter extends RecyclerView.Adapter<CategorieAdapter.MyViewHolder> {
    private ArrayList<Category> categories;

    Context context;

    FirebaseStorage firebaseStorage=FirebaseStorage.getInstance();
    FirebaseDatabase firebaseDatabase=FirebaseDatabase.getInstance();
    DatabaseReference rootRefCategory;
    DatabaseReference rootRefPhoto;



    public CategorieAdapter(ArrayList<Category> categories,Context context) {
        this.categories=categories;
        rootRefCategory=firebaseDatabase.getReference("category");
        rootRefPhoto=firebaseDatabase.getReference("photos");
        this.context=context;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.category_row_view,parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {


        holder.categoryName.setText(categories.get(position).categoryName);

        final ArrayList<Photos> photoList=new ArrayList<Photos>();
        final PhotoAdapter photoAdapter;
        StorageReference root=firebaseStorage.getReference("img");
        photoAdapter=new PhotoAdapter(photoList,root,context);
        GridLayoutManager gridLayoutManager=new GridLayoutManager(context,3);
        holder.photoListRecycleView.addItemDecoration(new GridSpacingItemDecoration(3,20,false));
        holder.photoListRecycleView.setLayoutManager(gridLayoutManager);
        holder.photoListRecycleView.setAdapter(photoAdapter);


        rootRefPhoto.child(categories.get(position).categoryName).limitToFirst(6).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot photos:dataSnapshot.getChildren()
                        ) {
                    Photos p=photos.getValue(Photos.class);
                    photoList.add(p);
                    System.out.println("value found..."+p.toString());
                }
                photoAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        RecyclerView photoListRecycleView;
        TextView categoryName;
        public MyViewHolder(View itemView) {
            super(itemView);
            photoListRecycleView=itemView.findViewById(R.id.recycle_view_photo);
            categoryName=itemView.findViewById(R.id.categoryName);
        }
    }
}