package com.aos.photos.Model;

import com.google.firebase.database.IgnoreExtraProperties;

import java.util.Map;

/**
 * Created by Arifur on 7/27/2017.
 */

@IgnoreExtraProperties
public class Photos {
    public String photosName;
    public int likeCount;
    public long timestatmp;

    public Photos() {
    }

    public Photos(String photosName, int likeCount, long timestatmp) {
        this.photosName = photosName;
        this.likeCount = likeCount;
        this.timestatmp = timestatmp;
    }

    @Override
    public String toString() {
        return "Photos{" +
                "photosName='" + photosName + '\'' +
                ", likeCount=" + likeCount +
                ", timestatmp=" + timestatmp +
                '}';
    }
}
