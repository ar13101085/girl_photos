package com.aos.photos.Fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aos.photos.Activity.MainViewActivity;
import com.aos.photos.Adapter.PhotoAdapter;
import com.aos.photos.Helper.GridSpacingItemDecoration;
import com.aos.photos.Model.Category;
import com.aos.photos.Model.Photos;
import com.aos.photos.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;


public class CategoryListFragment extends Fragment {


    public CategoryListFragment() {
    }


    FirebaseStorage firebaseStorage=FirebaseStorage.getInstance();
    FirebaseDatabase firebaseDatabase=FirebaseDatabase.getInstance();
    DatabaseReference rootRefCategory;
    DatabaseReference rootRefPhoto;
    RecyclerView recycle_view_photo;
    ArrayList<Photos> photoList=new ArrayList<Photos>();
    PhotoAdapter photoAdapter;
    Context context;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView=inflater.inflate(R.layout.main_view_layout,container,false);

        recycle_view_photo= (RecyclerView) rootView.findViewById(R.id.recycle_view_photo);


        rootRefCategory=firebaseDatabase.getReference("category");
        rootRefPhoto=firebaseDatabase.getReference("photos");
        StorageReference root=firebaseStorage.getReference("img");
        photoAdapter=new PhotoAdapter(photoList,root,context);

        GridLayoutManager gridLayoutManager=new GridLayoutManager(context,3);
        recycle_view_photo.addItemDecoration(new GridSpacingItemDecoration(3,20,false));
        recycle_view_photo.setLayoutManager(gridLayoutManager);
        recycle_view_photo.setAdapter(photoAdapter);

        return rootView;
    }

    public void LoadCategory(String categoryName){
        rootRefPhoto.child(categoryName).limitToFirst(6).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot photos:dataSnapshot.getChildren()
                        ) {
                    Photos p=photos.getValue(Photos.class);
                    photoList.add(p);
                    System.out.println("value found..."+p.toString());
                }
                photoAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

}
